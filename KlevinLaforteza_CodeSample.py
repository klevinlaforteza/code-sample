__author__ = "Klevin Laforteza"
__email__ = "lafortezaklevin@gmail.com"

import maya.cmds as mc
import maya.OpenMayaUI as omUI
from shiboken2 import wrapInstance
from PySide2 import QtCore, QtWidgets
from functools import partial
import logging

log = logging.getLogger(__name__)
log.setLevel(logging.WARNING)


# ----------------------------------------------------------------------------------------------------------------------

def maya_main_window():
    main_window_ptr = omUI.MQtUtil.mainWindow()
    return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)


class KLWindowUI(QtWidgets.QDialog):
    """
    Reusable window creation
    """

    def __init__(self, parent=maya_main_window()):
        super(KLWindowUI, self).__init__(parent)

    def set_window_name(self):
        self.window_name = 'KLWindowUI'
        self.setWindowTitle('KL Window Creator')

    def create_window(self):
        self.set_window_name()
        if mc.window(self.window_name, query=True, exists=True):
            mc.deleteUI(self.window_name, window=True)

        # Set window properties
        self.setObjectName(self.window_name)
        self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowContextHelpButtonHint)
        self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowMinMaxButtonsHint)

        self.create_ui()
        self.show()

    def create_ui(self):
        self.main_layout = QtWidgets.QHBoxLayout()
        self.setLayout(self.main_layout)


# ----------------------------------------------------------------------------------------------------------------------

# Stylesheet for the color index
STYLESHEET = {0: "qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(255, 255, 255, 255), stop:0.495 "
                 "rgba(255, 255, 255, 255), stop:0.505 rgba(0, 0, 0, 255), stop:1 rgba(0, 0, 0, 255));",
              1: "rgb(0, 0, 0)",
              2: "rgb(64, 64, 64)",
              3: "rgb(153, 153, 153)",
              4: "rgb(155, 0, 40)",
              5: "rgb(0, 4, 96)",
              6: "rgb(0, 0, 255)",
              7: "rgb(0, 70, 25)",
              8: "rgb(38, 0, 67)",
              9: "rgb(200, 0, 200)",
              10: "rgb(138, 72, 51)",
              11: "rgb(63, 35, 31)",
              12: "rgb(153, 38, 0)",
              13: "rgb(255, 0, 0)",
              14: "rgb(0, 255, 0)",
              15: "rgb(0, 65, 153)",
              16: "rgb(255, 255, 255)",
              17: "rgb(255, 255, 0)",
              18: "rgb(100, 220, 255)",
              19: "rgb(67, 255, 163)",
              20: "rgb(255, 176, 176)",
              21: "rgb(228, 172, 121)",
              22: "rgb(255, 255, 99)",
              23: "rgb(0, 153, 84)",
              24: "rgb(161, 106, 48)",
              25: "rgb(158, 161, 48)",
              26: "rgb(104, 161, 48)",
              27: "rgb(48, 161, 93)",
              28: "rgb(48, 161, 161)",
              29: "rgb(48, 103, 161)",
              30: "rgb(111 48, 161)",
              31: "rgb(161, 48, 106)",
              }


def color_override(index):
    """
    Sets the override color of curves
    :param index: (int) 0-31 based on default maya index colors
    :return: None
    """
    # Query the selected objects
    selection = mc.ls(selection=True, long=True)

    # Loop through the selected objects
    if selection:
        for i in selection:
            # Get the shape nodes of object
            shapes = mc.listRelatives(i, shapes=True, fullPath=True, type="nurbsCurve")
            if shapes:
                # Enable and set the override color for each shape
                for shape in shapes:
                    mc.setAttr('%s.overrideEnabled' % shape, True)
                    mc.setAttr('%s.overrideColor' % shape, index)
    else:
        log.warning("No selection.")


class KLColorOverride(KLWindowUI):

    def __init__(self, parent=kl_window.maya_main_window()):
        super(KLColorOverride, self).__init__(parent)
        log.debug("Color Override")

    def set_window_name(self):
        self.window_name = 'KLColorOverrideUI'
        self.setWindowTitle('KL Color Override')

    def create_ui(self):
        self.main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.main_layout)

        color_grid_layout = QtWidgets.QGridLayout()
        color_grid_layout.setSpacing(0)
        self.main_layout.addLayout(color_grid_layout)

        # Create 16x2 grid buttons layout for index color
        row = 0
        column = 0
        for i in range(32):
            # Create button and set its attributes
            button = QtWidgets.QPushButton()
            button.setFixedSize(20, 20)
            button.setStyleSheet("background-color: %s" % STYLESHEET[i])
            button.clicked.connect(partial(color_override, i))
            color_grid_layout.addWidget(button, row, column)

            column += 1
            if column > 15:
                row += 1
                column = 0

        log.debug("Successfully created color override ui")


# ----------------------------------------------------------------------------------------------------------------------


class CheckOverlapUV:

    def __init__(self):
        log.debug("Check Overlap UV")

    def overlap_uv(self):
        selection = mc.ls(selection=True, long=True)
        geos = []
        faces = []
        if selection:
            for i in selection:
                if mc.listRelatives(i, shapes=True, path=True):
                    materials = self.get_materials(i)
                    if len(materials) > 1:
                        for m in materials:
                            select_faces = self.select_faces(m, i)
                            overlap = mc.polyUVOverlap(select_faces, overlappingComponents=True)

                            if overlap:
                                faces.append(overlap)
                                geos.append(i)
                    else:
                        overlap = mc.polyUVOverlap('%s.f[0:]' % i, overlappingComponents=True)
                        if overlap:
                            faces.append(overlap)
                            geos.append(i)
        mc.select(selection)
        return geos, faces

    def get_materials(self, mesh):
        """
        Gets the materials applied to set object
        :param mesh: (str) mesh node
        :return: (str) name of materials applied to mesh
        """
        mc.select(mesh)

        # Select materials in hypershade
        mc.hyperShade(shaderNetworksSelectMaterialNodes=True)
        materials = mc.ls(selection=True)

        return materials

    def select_faces(self, material, mesh):
        """
        Gets the faces that has the material applied to
        :param material: (str) name of the material
        :param mesh: (str) name of mesh to query faces with applied material
        :return: (list) face index that has material applied to
        """
        # Get material's shading engine
        sg_con = mc.listConnections('%s.outColor' % material, source=False, destination=True)
        if sg_con[0] == 'initialParticleSE':
            sg = sg_con[1]
        else:
            sg = sg_con[0]

        # Select faces with applied material
        faces = []
        for i in mc.sets(sg, query=True):
            if mesh not in i:
                continue
            faces.append(i)

        return faces


# ----------------------------------------------------------------------------------------------------------------------

def label_joints(root="global_C0_0_jnt"):
    """
    Sets the joint label for the left and right joints for mirroring skin weights
    :return:
    """
    # Get all the joints from the root hierachy
    joints = mc.listRelatives(root, allDescendents=True, type="joint")
    for i in joints:
        name, side, num = i.split("_")[:3]

        # Check if the joint index is an integer
        try:
            num = int(num)
        except:
            pass
        if isinstance(num, int):
            name = "%s_%s" % (name, str(num))

        # Define the joint side
        joint_side = 1 if side.startswith("L") else 2 if side.startswith("R") else 0
        if joint_side:
            mc.setAttr("%s.side" % i, joint_side)
            mc.setAttr("%s.type" % i, 18)
            mc.setAttr("%s.otherType" % i, name, type="string")
